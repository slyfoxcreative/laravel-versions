<?php

declare(strict_types=1);

namespace SlyFoxCreative\Versions\Tests;

use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\PackageManagers\PackageSet;

class CommandTest extends TestCase
{
    public function testPhpCommand()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(200, [], $this->fixture('composer-semver.json')),
            // https://repo.packagist.org/p2/guzzlehttp/guzzle.json
            new Response(200, [], $this->fixture('guzzlehttp-guzzle.json')),
            // https://repo.packagist.org/p2/illuminate/support.json
            new Response(200, [], $this->fixture('illuminate-support.json')),
            // https://repo.packagist.org/p2/laravel/framework.json
            new Response(200, [], $this->fixture('laravel-framework.json')),
            // https://repo.packagist.org/p2/psr/log.json
            new Response(200, [], $this->fixture('psr-log.json')),
            // https://repo.packagist.org/p2/slyfoxcreative/coding-styles.json
            new Response(404, [], 'Not found'),
            // https://composer.slyfoxcreative.com/p2/slyfoxcreative/coding-styles.json
            new Response(200, [], $this->fixture('slyfoxcreative-coding-styles.json')),
        );

        PackageSet::setHttpClient($client);

        $this
            ->artisan('versions:php', [
                '--file' => $this->fixturesPath('composer.json'),
                '--lockfile' => $this->fixturesPath('composer.lock'),
            ])
            ->expectsOutput('Packages...')
            ->expectsTable(
                ['Package', 'Constraint', 'Current', 'Latest', 'Outdated', 'Error'],
                [
                    ['composer/semver', '^3.2', '3.2.5.0', '3.2.5.0', '', ''],
                    ['guzzlehttp/guzzle', '^7.3', '7.3.0.0', '7.3.0.0', '', ''],
                    ['illuminate/support', '^8.54', '?', '8.54.0.0', '', 'Replaced by laravel/framework'],
                    ['laravel/framework', '^8.54', '8.54.0.0', '8.54.0.0', '', ''],
                    ['psr/log', '^1.0', '1.1.4.0', '3.0.0.0', 'X', ''],
                    ['slyfoxcreative/coding-styles', '^1.2', '1.2.0.0', '1.2.0.0', '', ''],
                ],
            )
            ->assertExitCode(0)
        ;
    }

    public function testJavaScriptCommand()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/bootstrap
            new Response(200, [], $this->fixture('bootstrap.json')),
            // https://api.npms.io/v2/package/jquery
            new Response(200, [], $this->fixture('jquery.json')),
            // https://api.npms.io/v2/package/popper.js
            new Response(200, [], $this->fixture('popper.js.json')),
            // https://api.npms.io/v2/package/stimulus
            new Response(200, [], $this->fixture('stimulus.json')),
        );

        PackageSet::setHttpClient($client);

        $this
            ->artisan('versions:javascript', [
                '--file' => $this->fixturesPath('package.json'),
                '--lockfile' => $this->fixturesPath('yarn1.lock'),
            ])
            ->expectsOutput('Packages...')
            ->expectsTable(
                ['Package', 'Constraint', 'Current', 'Latest', 'Outdated', 'Error'],
                [
                    ['bootstrap', '^4.6', '4.6.0.0', '5.1.0.0', 'X', 'API reports an error parsing metadata'],
                    ['jquery', '^3.6', '3.6.0.0', '3.6.0.0', '', ''],
                    ['popper.js', '^1.16', '1.16.1.0', '1.16.1.0', '', ''],
                    ['stimulus', '^2.0', '2.0.0.0', '2.0.0.0', '', ''],
                ],
            )
            ->assertExitCode(0)
        ;
    }
}
