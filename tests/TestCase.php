<?php

declare(strict_types=1);

namespace SlyFoxCreative\Versions\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Orchestra\Testbench\TestCase as BaseTestCase;
use SlyFoxCreative\Versions\ServiceProvider;

class TestCase extends BaseTestCase
{
    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
        ];
    }

    /**
     * Returns the absolute path to a file in the tests/fixtures directory.
     */
    protected function fixturesPath(string $path): string
    {
        return getcwd() . "/tests/fixtures/{$path}";
    }

    /**
     * Returns the contents of a file in the tests/fixtures directory.
     */
    protected function fixture(string $path): string
    {
        return file_get_contents($this->fixturesPath($path));
    }

    /**
     * Returns a Guzzle Client with a mock handler.
     */
    protected function mockGuzzleClient(...$responses): Client
    {
        $mockHandler = new MockHandler($responses);
        $handlerStack = HandlerStack::create($mockHandler);

        return new Client(['handler' => $handlerStack]);
    }
}
