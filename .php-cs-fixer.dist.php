<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
;

return SlyFoxCreative\CodingStyles\config($finder);
