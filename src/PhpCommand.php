<?php

declare(strict_types=1);

namespace SlyFoxCreative\Versions;

use Illuminate\Console\Command;
use SlyFoxCreative\PackageManagers\Commands\PhpHandler;

class PhpCommand extends Command
{
    use PhpHandler;

    protected $signature = 'versions:php {--file=composer.json} {--lockfile=composer.lock}';

    protected $description = 'Show version information for PHP packages';
}
