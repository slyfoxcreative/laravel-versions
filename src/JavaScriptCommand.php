<?php

declare(strict_types=1);

namespace SlyFoxCreative\Versions;

use Illuminate\Console\Command;
use SlyFoxCreative\PackageManagers\Commands\JavaScriptHandler;

class JavaScriptCommand extends Command
{
    use JavaScriptHandler;

    protected $signature = 'versions:javascript {--file=package.json} {--lockfile=yarn.lock}';

    protected $description = 'Show version information for JavaScript packages';
}
